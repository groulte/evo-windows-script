<#
SCRIPT CONF VM MS
ERIC .G
#>


$pause_install = "60"

#$VM_name = "Test-deb-80"
$VM_name = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.hostname" 2>&1 | Out-String
$VM_name = $VM_name -replace "`t|`n|`r",""

# Si nom de VM nul ou trop long, message d'erreur

if($VM_name.length -gt 15 -Or $VM_name.length -eq 0)
{
	Write-Host "`n Le nom de la VM doit �tre compris entre 1 et 11 caract�res`r`n Assignation du nom par d�faut : DC-1"
	$VM_Name = "DC-1"
}


#$ip_address = "172.20.12.80"
$ip_address = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.ipaddress" 2>&1 | Out-String
$ip_address = $ip_address -replace "`t|`n|`r",""

#$ip_netmask = "255.255.255.0"
$ip_netmask = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.netmask" 2>&1 | Out-String
$ip_netmask = $ip_netmask -replace "`t|`n|`r",""

#$ip_gateway = "172.20.12.254"
$ip_gateway = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.gateway" 2>&1 | Out-String
$ip_gateway = $ip_gateway -replace "`t|`n|`r",""

#$ip_dns1 = "172.20.0.20"
$ip_dns1 = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.dns1" 2>&1 | Out-String
$ip_dns1 = $ip_dns1 -replace "`t|`n|`r",""

#$ip_dns2 = "8.8.8.8"
$ip_dns2 = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.dns2" 2>&1 | Out-String
$ip_dns2 = $ip_dns2 -replace "`t|`n|`r",""


$salt_exe = "Salt-Minion-2015.5.5-AMD64-Setup.exe"
#$salt_master = "172.20.10.170"
$salt_master = C:\Progra~1\VMware\VMware~1\vmtoolsd.exe --cmd "info-get guestinfo.salt_master" 2>&1 | Out-String
$salt_master = $salt_master -replace "`t|`n|`r",""


Write-Output "CONF VM : $VM_name $ip_address $ip_netmask $ip_gateway $ip_dns1 $ip_dns2" > "C:\\Windows\\Setup\\Scripts\\log.txt"
Write-Output "CONF SALT : $salt_exe $salt_master $VM_name" >> "C:\\Windows\\Setup\\Scripts\\log.txt"

#INSTALL MINION
C:\\INSTALL\\SOURCES\\Salt-Minion-2015.5.0-2-AMD64-Setup.exe /S /master="$salt_master" /minion-name=$VM_name

Start-Sleep -s $pause_install >> "C:\\Windows\\Setup\\Scripts\\log.txt"
net start salt-minion >> "C:\\Windows\\Setup\\Scripts\\log.txt"

#CHANGE NETWORK CONFIGURATION
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set address ""Ethernet"" static $ip_address $ip_netmask $ip_gateway 1"
Start-Sleep -s 10
cmd.exe /C "c:\windows\system32\netsh.exe interface ip set dnsservers ""Ethernet"" static $ip_dns1 primary"
cmd.exe /C "c:\windows\system32\netsh.exe interface ip add dnsservers ""Ethernet"" $ip_dns2"

#DELETE FILE UNATTEND.XML
cmd.exe /C "del /q /f c:\windows\system32\sysprep\unattend.xml" 
cmd.exe /C "del /q /f c:\windows\panther\unattend.xml"

New-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce\" -Name "read_role" -Value "$pshome\powershell.exe -file C:\SCRIPTS\Scripts_PS\read_role.ps1" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "AutoAdminLogon" -Value "1" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultDomainName" -Value ".\" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultUserName" -Value "admin" -Force
New-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Winlogon\" -Name "DefaultPassword" -Value "P@ssw0rd44" -Force

Rename-Computer -NewName $VM_name -Force -Restart